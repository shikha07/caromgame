#include <iostream>
#include <cmath>
#include<stdio.h>
#include <GL/glut.h>
#include <sys/time.h>
#include<string.h>	

using namespace std;

#define PI 3.141592653589
#define DEG2RAD(deg) (deg * PI / 180)

// Function Declarations
void drawScene();
void update(int value);
void drawBox(float len);
void drawBall(float rad);
void drawTriangle();
void initRendering();
void handleResize(int w, int h);
void handleKeypress1(unsigned char key, int x, int y);
void handleKeypress2(int key, int x, int y);
void handleMouseclick(int button, int state, int x, int y);
void drawSquare(float len);
void drawCircle(float rad);
void myMouse(int b, int s, int x, int y) ;
void motion( int x, int y );
void checkTime(int value);



class Coin{
	public:
		double x;
		double y;
		double vel_x;
		double vel_y;
		double vel;
		Coin(double vx, double vy);
		void update_pos( double x1, double y1);
		void update_vel( double v_x, double v_y);
};

class Striker: public Coin{
	public:
		double rad;
		double fake_vel,angle;
		void reinitialise(void);
		Striker(double vx, double vy, double rad1):Coin(vel_x, vel_y),rad(rad1){
			vel_x = vx;
			vel_y= vy;
			angle = 0.0f;
			fake_vel = 0.01f;
			rad = rad1;
			x=0;
			y=-0.96f;
		}
		void collision(Coin* coin);
		void set_fake(double v);
		void set_angle(double ang);
		void show_angle(void);
		void show_vel(void);
		void shoot(void);
};

class GamePlay{
	public:
		int coins;
		Coin* queen;
		int count[3];
		double friction;
		int points[3];
		int turn;
		double rad;
		int color;
		Coin* white[10];
		Coin* black[10];
		GamePlay(int coins1, double friction1, int points1);
		void drawBoard();
		void loadCoins();
		void initialiseCoins();

};


// Global Variables
//float theta = 0.0f; 
GamePlay* game = new GamePlay(0,0.0001,40); 
Striker* striker = new Striker(0.0f,0.0f,0.12f);
int active = 0;
int up=0;
int yes = 0;
int rememberqueen = 0;
struct timeval tv_prev,tv_now, tv_init;
int reinit =1 ;
int multi = 0;
int p = 0;
int q=0;

void drawText(char* text,float x, float y , float z)
{
	char *c;
	glPushMatrix();
	glTranslatef(x,y ,z);
	glLineWidth(2.0);
	glScalef(0.40/152.38, 0.40/152.38, 1/152.38);
	for (c=text; *c != '\0'; c++)
	{
		glutStrokeCharacter(GLUT_STROKE_ROMAN , *c);
	}
	glPopMatrix();
}


void scoredisplay(void)
{
	long long int i =0,g= game->points[1];
	int t=0;
	char *c,score[100]="Score - ";
	char sc[1000];
	glColor3f(1.0, 0.30, 0.00);
	glLineWidth(1.5);
	if( multi )
	{
		glPushMatrix();
		char* coe;	
		if( game->turn == 0  && multi){
			if(  game->color == 1 )
				coe = "Player 1: White";
			else
				coe = "Player 1: Black";

			glTranslatef(-4.0f,3.0f ,-8.0f);
		}	
		else if(multi) {

			if(  game->color == 2 )
				coe = "Player 2: White";
			else
				coe = "Player 2: Black";
			glTranslatef(1.0f,3.0f ,-8.0f);
		}
		

		if(multi){
			glScalef(0.30/152.38, 0.30/152.38, 1/152.38);
			for (c=coe; *c != '\0'; c++)
			{
				glutStrokeCharacter(GLUT_STROKE_ROMAN , *c);
			}
			glPopMatrix();
		}

//		glColor3f(0.40, 01.0, 0.00);
	
//		glColor3f(1.0, 0.50, 0.00);
		glPushMatrix();
		glLineWidth(1.5);
		glTranslatef(4.0f,1.0f ,-10.0f);
		glScalef(0.30/152.38, 0.30/152.38, 1/152.38);
		for (c="Player 2:"; *c != '\0'; c++)
		{
			glutStrokeCharacter(GLUT_STROKE_ROMAN , *c);
		}

		glPopMatrix();

		glPushMatrix();
		g= game->points[2];
		glLineWidth(1.5);
		glTranslatef(4.0f,0.50f ,-10.0f);
		glScalef(0.30/152.38, 0.30/152.38, 1/152.38);
		for (c="Score - "; *c != '\0'; c++)
		{
			glutStrokeCharacter(GLUT_STROKE_ROMAN , *c);
		}
		if( g<0)
		{
			g*=-1;
			c="-\0";
			glutStrokeCharacter(GLUT_STROKE_ROMAN , *c);
		}

		while(g){
			score[i]=g%10 + '0';
			g/=10;
			i++;
		}

		if(i==0)
			score[i++]='0';
		while(i--)
			sc [t++]=score[i];
		sc[t]='\0';	
		score[i] ='\0';
		for (c=sc; *c != '\0'; c++)
		{
			glutStrokeCharacter(GLUT_STROKE_ROMAN , *c);
		}
		glPopMatrix();


		glPushMatrix();
		char soe[19]= "Coins pocketed - ";
		glTranslatef(4.0f,0.0f ,-10.0f);
		glScalef(0.30/152.38, 0.30/152.38, 1/152.38);
		for (c=soe; *c != '\0'; c++)
		{
			glutStrokeCharacter(GLUT_STROKE_ROMAN , *c);
		}

		i =0, g= game->count[2];

		while(g){
			score[i]=g%10 + '0';
			g/=10;
			i++;
		}
		t=0;
		if(i==0)
			score[i++]='0';
		while(i--)
			sc [t++]=score[i];
		sc[t]='\0';

		for (c=sc; *c != '\0'; c++)
		{
			glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN , *c);
		}
		glPopMatrix();


		glPushMatrix();
		glLineWidth(1.5);
		glTranslatef(-7.0f,1.0f ,-10.0f);
		glScalef(0.30/152.38, 0.30/152.38, 1/152.38);
		for (c="Player 1:"; *c != '\0'; c++)
		{
			glutStrokeCharacter(GLUT_STROKE_ROMAN , *c);
		}

		glPopMatrix();


	}
	g = game->points[1];
//	glColor3f(01.0, 0.30, 0.00);
	glPushMatrix();

	glLineWidth(1.5);
	glTranslatef(-7.0f,0.50f ,-10.0f);
	glScalef(0.30/152.38, 0.30/152.38, 1/152.38);
	for (c="Score - "; *c != '\0'; c++)
	{
		glutStrokeCharacter(GLUT_STROKE_ROMAN , *c);
	}
	if( g<0)
	{
		g*=-1;
		c="-\0";
		glutStrokeCharacter(GLUT_STROKE_ROMAN , *c);
	}
	i=0;
	while(g){
		score[i]=g%10 + '0';
		g/=10;
		i++;
	}

	if(i==0)
		score[i++]='0';
	t=0;
	while(i--)
		sc [t++]=score[i];
	sc[t]='\0';	
	score[i] ='\0';
	for (c=sc; *c != '\0'; c++)
	{
		glutStrokeCharacter(GLUT_STROKE_ROMAN , *c);
	}
	glPopMatrix();
	glPushMatrix();

	if( !multi )
	{
		if( game->coins != game->count[game->turn+1] || game->coins == 0)
			gettimeofday(&tv_now, NULL);
		char scoe[19]= "Time Elapsed - ";
		glTranslatef(-7.0f,-2.0f ,-10.0f);
		glScalef(0.30/152.38, 0.30/152.38, 1/152.38);
		for (c=scoe; *c != '\0'; c++)
		{
			glutStrokeCharacter(GLUT_STROKE_ROMAN , *c);
		}


		i =0, g= tv_now.tv_sec - tv_init.tv_sec;
		while(g){
			score[i]=g%10 + '0';
			g/=10;

			i++;

		}
		t=0;
		if(i==0)
			score[i++]='0';
		while(i--)
			sc [t++]=score[i];
		sc[t++]='s';	
		sc[t]='\0';	

		for (c=sc; *c != '\0'; c++)
		{
			glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN , *c);
		}
		glPopMatrix();
		glPushMatrix();
		char* coe;
		if( game->color == 1)
			coe = "Your color - white";
		else
			coe = "Your color - black";
		glTranslatef(-6.0f,2.5f ,-10.0f);
		glScalef(0.30/152.38, 0.30/152.38, 1/152.38);
		for (c=coe; *c != '\0'; c++)
		{
			glutStrokeCharacter(GLUT_STROKE_ROMAN , *c);
		}
		glPopMatrix();
	}
	glPushMatrix();
	//	char soe[19]= "Coins pocketed - ";
	glTranslatef(-7.0f,0.00f ,-10.0f);
	glScalef(0.30/152.38, 0.30/152.38, 1/152.38);
	for (c="Coins pocketed -"; *c != '\0'; c++)
	{
		glutStrokeCharacter(GLUT_STROKE_ROMAN , *c);
	}

	i =0, g= game->count[1];

	while(g){
		score[i]=g%10 + '0';
		g/=10;
		i++;
	}
	t=0;
	if(i==0)
		score[i++]='0';
	while(i--)
		sc [t++]=score[i];
	sc[t]='\0';	

	for (c=sc; *c != '\0'; c++)
	{
		glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN , *c);
	}
	glPopMatrix();
	if( rememberqueen == -5 )
	{
		glPushMatrix();
		//		char soe[19]= "Queen pocketed!!";
		if( q == 0)
			glTranslatef(-7.0f,-2.5f ,-10.0f);
		else
			glTranslatef(4.0f,-2.5f ,-10.0f);
	
		glScalef(0.30/152.38, 0.30/152.38, 1/152.38);
		for (c="Queen pocketed!!"; *c != '\0'; c++)
		{
			glutStrokeCharacter(GLUT_STROKE_ROMAN , *c);
		}
		glPopMatrix();


	}

}

void start(void){

	if( !p ){
		drawText("Choose Player: ",-3.40f,1.0f,-8.0f);
		drawText("o one",-3.20f, 0.0f, -8.0f);
		drawText("t two",-3.20f, -0.50f, -8.0f);
	}
	else if( game->color == 0 )
	{
		drawText("Choose color",-3.40f, 1.0f, -8.0f);
		drawText("b black",-3.20f, 0.0f, -8.0f);
		drawText("w white",-3.20f, -0.50f, -8.0f);
	}

	else if( game->coins == 0 )
	{
		drawText("Enter number of coins:",-3.40f,1.0f,-8.0f);
		drawText("3",-3.20f, 0.0f, -8.0f);
		drawText("4",-3.20f, -0.50f, -8.0f);
		drawText("5",-3.20f, -1.0f, -8.0f);
		drawText("6",-3.20f, -1.50f, -8.0f);
	}
	else 
	{
		drawText("Surface Frcition",-3.40f,1.0f,-8.0f);
		drawText("h rough",-3.20f, 0.0f, -8.0f);
		drawText("m medium",-3.20f, -0.50f, -8.0f);
		drawText("l smooth",-3.20f, -1.0f, -8.0f);
	}	
}

int main(int argc, char **argv) {

	// Initialize GLUT
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	int w = glutGet(GLUT_SCREEN_WIDTH);
	int h = glutGet(GLUT_SCREEN_HEIGHT);
	int windowWidth = w * 2 / 3;
	int windowHeight = h * 2 / 3;

	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition((w - windowWidth) / 2, (h - windowHeight) / 2);

	glutCreateWindow("CSE251_sampleCode");  // Setup the window
	initRendering();

	// Register callbacks
	glutDisplayFunc(drawScene);
	glutIdleFunc(drawScene);
	glutKeyboardFunc(handleKeypress1);
	glutSpecialFunc(handleKeypress2);
	glutMouseFunc(myMouse);
	glutMotionFunc(motion);
	glutReshapeFunc(handleResize);
	glutTimerFunc(15, checkTime, 0);
	glutMainLoop();
	return 0;
}

void result(void){

	drawText("Game over!",1.0,0.0,-10.0);
	scoredisplay();
	if( multi )
	{
		if( game->points[1] > game->points[2] )
			drawText("Player 1 WON",-1.0,-2.0,-8.0);
		else if( game->points[1] < game->points[2] )
			drawText("Player 2 WON",-1.0,-2.0,-8.0);
		else
			drawText("Game draw",-1.0,-2.0,-8.0);

	}	
        drawText("Play Again y yes n no",1.0,-3.0,-8.0);        

}
// Function to draw objects on the screen
void drawScene() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
	
	if( !yes )
		start();
	else if(game->count[1] == game->coins || game->count[2] == game->coins )
		result();
	else{
		game->drawBoard();
		game->loadCoins();
		scoredisplay();
	}
	glutSwapBuffers();
}



Coin::Coin(double vx, double vy){
	vel_x = vx;
	vel_y = vy;
	vel = sqrt(vx*vx+vy*vy);
}

void Coin::update_pos( double x1, double y1){
	x = x1;
	y = y1;
}

void Coin::update_vel( double v_x, double v_y){
	vel_x = v_x;
	vel_y = v_y;
	vel = sqrt(vel_x*vel_x + vel_y*vel_y);
}

void Striker::reinitialise(void){
	update_pos(0.0f,-0.96f);
	update_vel(0.0f,0.0f);
	fake_vel =0.01f;
	angle=0.0f;
}

void Striker::set_fake(double v){
	fake_vel = v;
}

void Striker::set_angle(double ang){
	angle = ang;
}

void Striker::show_vel(void){
	glColor3f(0.8, 0.0, 0.0);
	glLineWidth(2.5);
	double i = fake_vel;

	drawText("speed",-3.40f,1.0f,-8.0f);

	glColor3f(0.0, 1.0, 0.0);
	glLineWidth(2.5);

	glBegin(GL_LINES);
	while(i>0.0f){
		glVertex3f(-1.5f, -0.0f+i*10, -5.0f);
		glVertex3f(-2.0f, -0.0f+i*10, -5.0f);
		i-=0.01f;
	}
	glEnd();
}

void Striker::shoot(void){
	update_vel(fake_vel*sin(DEG2RAD(angle)), fake_vel*cos(DEG2RAD(angle)));
}

void Striker::show_angle(void){
  glMatrixMode(GL_MODELVIEW);
                          glLoadIdentity();


	glColor3f(0.8, 0.0, 0.0);
	glLineWidth(2.5);
	glColor3f(0.0, 1.0, 0.0);
	glLineWidth(2.5);
	glBegin(GL_LINES);
	glVertex3f(x, y, -6.0f);
	glVertex3f(x+2.0f*sin(DEG2RAD(angle)), y+2.0f*cos(DEG2RAD(angle)), -6.0f);
	glEnd();
			glutSwapBuffers();


}
GamePlay::GamePlay(int coins1, double friction1, int points1){
	int i;
	coins = coins1;
	friction = friction1;
	points[1] = points1;
	points[2] = points1;
	turn = 0;
	rad = 0.075f;
	//	striker = new Striker(0,0,0.15f);
	queen = new Coin(0,0);
	for( i = 0 ; i < 10 ; i++ )
	{
		black[i] = new Coin(0,0);
		white[i] = new Coin(0,0);
	}
	color = 0;
	count[1] = 0;
	count[2] = 0;
	initialiseCoins();
}

void GamePlay::drawBoard(){

	int i,j;
	glPushMatrix();
	glTranslatef(0.0f, 0.0f, -10.0f);
	glColor3f(0.07f, 0.0f, 0.0f);
	drawSquare(6.5f);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.0f , -8.0f);
	glColor3f(0.5f, 0.25f, 0.15f);
	drawSquare(4.0f);
	glPopMatrix();
	// Pockets

	for(i=-1;i<1;i++){
		if(i==0)
			i++;

		for(j=-1;j<1;j++){
			if( j == 0 )
				j++;
			glPushMatrix();
			glTranslatef(1.35f*i, 1.35f*j, -6.0f);
			glColor3f(0.01f, 0.0f, 0.0f);
			drawBall(2.10f*rad);
			glPopMatrix();
			glPushMatrix();	
			glColor3f(0.8, 0.0, 0.0);
			glTranslatef(-1.15f*i, -0.95f*j, -7.0f);
			drawBall(3*rad/2);
			glPopMatrix();
			glPushMatrix();
			glTranslatef(-0.95f*i, -1.15f*j, -7.0f);	
			drawBall(3*rad/2);
			glPopMatrix();
		}
	}

	//     Line Decorn
	glColor3f(0.8, 0.0, 0.0);
	glLineWidth(2.5);
	glBegin(GL_LINES);
	glVertex3f(-1.25f, -1.13f+3*rad, -7.0f);
	glVertex3f(-1.25f, 1.13f-3*rad, -7.0f);
	glVertex3f(1.25f, -1.13f+3*rad, -7.0f);
	glVertex3f(1.25f, 1.13f-3*rad, -7.0f);
	glVertex3f( 1.13f-3*rad,-1.25f, -7.0f);
	glVertex3f(-1.13f+3*rad,-1.25f, -7.0f);
	glVertex3f(1.13f-3*rad, 1.25f, -7.0f);
	glVertex3f(-1.13f+3*rad, 1.25f, -7.0f);
	glEnd();

	//center decorn
	glPushMatrix();
	glTranslatef(-0.0f, 0.0f, -7.0f);
	glColor3f((255.0f/255.0f), (69.0f)/(255.0f), (0.0f)/(255.0f));
	glLineWidth(2.5);
	drawCircle(5*rad);
	drawCircle(4*rad);
	drawCircle(9*rad);
	drawCircle(10*rad);
	glPopMatrix();
}

void GamePlay::initialiseCoins(){
	int i,rotationAngle=0;
	// draw queen
	queen->update_pos(0,0);
	for(i=0;i<3;i++){
		white[i]->update_pos(2.1*rad*sin(DEG2RAD(rotationAngle)),2.1*rad*cos(DEG2RAD(rotationAngle)));
		rotationAngle+=60;	
		black[i]->update_pos(2.1*rad*sin(DEG2RAD(rotationAngle)),2.1*rad*cos(DEG2RAD(rotationAngle)));
		rotationAngle+=60;
	}
	rotationAngle=30;
	for( ; i < 6 ; i++){
		white[i]->update_pos(3.8*rad*sin(DEG2RAD(rotationAngle)),3.8*rad*cos(DEG2RAD(rotationAngle)));
		rotationAngle+=60;
		black[i]->update_pos(3.8*rad*sin(DEG2RAD(rotationAngle)),3.8*rad*cos(DEG2RAD(rotationAngle)));
		rotationAngle+=60;

	}
}

void GamePlay::loadCoins(){
	int i;
	// draw queen
	glPushMatrix();
	glTranslatef(queen->x, queen->y, -6.0f);
	glColor3f(1.0f, 0.0f, 0.1f);
	drawBall(rad);
	glPopMatrix();

	for(i=0;i<coins;i++){
		glPushMatrix();
		glTranslatef(white[i]->x,white[i]->y,-6.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		drawBall(rad);
		glPopMatrix();
		glPushMatrix();
		glTranslatef(black[i]->x,black[i]->y,-6.0f);
		glColor3f(00.0f, 0.0f, 0.0f);
		drawBall(rad);
		glPopMatrix();

	}
	glPushMatrix();
	glTranslatef(striker->x, striker->y, -6.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	drawBall(striker->rad);
	glPopMatrix();

}

void drawSquare(float len) {

	//	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glBegin(GL_QUADS);
	glVertex2f(-len / 2, -len / 2);
	glVertex2f(len / 2, -len / 2);
	glVertex2f(len / 2, len / 2);
	glVertex2f(-len / 2, len / 2);
	glEnd();
	//	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void drawCircle(float rad){

	glBegin(GL_LINE_STRIP);
	for (int i=0; i < (360); i++) {
		float degInRad = DEG2RAD(i);
		glVertex2f(cos(degInRad)*rad,sin(degInRad)*rad);
	}
	glEnd();
}


void apply_friction(Coin* c){

	if( c->vel ){

		if( fabs(c->vel_x) > fabs(game->friction*(c->vel_x/c->vel)) )
			c->vel_x -= game->friction*(c->vel_x/c->vel);
		else
			c->vel_x = 0.0f;
		if( fabs(c->vel_y) > fabs(game->friction*(c->vel_y/c->vel)) )
			c->vel_y -= game->friction*(c->vel_y/c->vel);
		else
			c->vel_y = 0.0f;

		c->vel = sqrt( pow(c->vel_x,2) + pow(c->vel_y,2));

		if( c->vel < 0.01f )
			c->update_vel(0.0f,0.0f);
	}
}

float world=-0.10f;
void check_hole(int type, Coin* c){


	if( pow(fabs(c->x) - 1.35f, 2)+pow( fabs(c->y) - 1.35f ,2) <= pow(2*game->rad,2))
	{	
		c->update_vel(0.0f,0.0f);
		c->update_pos(5.0f, 5.0f);
		if( (game->color == type && multi && game->turn == 0)|| (game->color != type && multi && game->turn == 1 && type != 3) || (!multi && game->color == type)){
			if( game->count[game->turn + 1] == game->coins - 1 && rememberqueen != -5 )
			{
				c->update_pos(world, world);
				world+=0.1f;
				if( world > 0.3f )
					world = -0.3f;
			}
			else
			{
				game->points[game->turn + 1] += 10;
				game->count[game->turn+1]++;
			}
		}
		else if( type == 1 || type == 2 )
		{
			game->points[game->turn+1] -= 5;
			c->update_pos(world, world);
			world+=0.1f;
			if( world > 0.3f )
				world = -0.3f;
		}
		else if( type == 3 )
		{
			q = game->turn;
			rememberqueen = -5;
			game->points[game->turn+1]+=50;
		}
	}
}

void change_coords(Coin* c){
	c->x += c->vel_x;
	c->y += c->vel_y;
	if( fabs(c->x) > 1.39f )
	{
		c->vel_x *= -1;
		c->x += c->vel_x;
	}
	if( fabs(c->y) > 1.39f )
	{
		c->vel_y *= -1;
		c->y += c->vel_y;	
	}
}

void collision(Coin* c1, Coin* c2 ,float m1, float m2){
 float x,y,u1,u2,v1,v2,q1,q2,z,e=1;
        x = c1->x - c2->x;
        y = c1->y - c2->y;
        z=sqrt(x*x + y*y);
        u1=(x*c1->vel_x + y*c1->vel_y)/z;
        u2=(x*c2->vel_x + y*c2->vel_y)/z;
        v2=(u2*(m2-e*m1)+(1+e)*m1*u1)/(m1+m2);
        v1=(u1*(m1-e*m2)+(1+e)*m2*u2)/(m1+m2);
        q1=(y*c1->vel_x - x*c1->vel_y)/z;
        q2=(y*c2->vel_x - x*c2->vel_y)/z;
        c1->vel_x = (v1*x + q1*y)/z;
        c1->vel_y = (v1*y - q1*x)/z;
        c2->vel_x = (v2*x + q2*y)/z;
        c2->vel_y = (v2*y - q2*x)/z;
        c1->update_vel(-c1->vel_x, -c1->vel_y);
        c2->update_vel(-c2->vel_x, -c2->vel_y);

}

void check_reinitialise(void){

	int i;
	for( i = 0; i<game->coins ; i++)
	{
		if( (fabs(game->white[i]->x ) > 1.39f && game->white[i]->x != 5.0f ) || (fabs(game->white[i]->y) > 1.39f  && game->white[i]->y != 5.0f))
		{
			game->white[i]->update_pos(world,world);
			game->white[i]->update_vel(0.0f,0.0f);
			world+=0.1f;
			if(world >0.3f)
				world = -0.3f ;
		}
		if( (fabs(game->black[i]->x ) > 1.39f && game->black[i]->x != 5.0f ) || (fabs(game->black[i]->y) > 1.39f  && game->black[i]->y != 5.0f))
		{
			game->black[i]->update_pos(world,world);
			game->black[i]->update_vel(0.0f,0.0f);
			world+=0.1f;
			if(world >0.3f)
				world = -0.3f;
		}
	}
	for(i = 0; i < game->coins ;i++ )
	{
		if( game->white[i]->vel || game->black[i]->vel )
		{
			reinit = 1;
			break;
		}
	}

	if( i == game->coins && reinit && !game->queen->vel && !striker->vel){
		reinit = 0;
		if( rememberqueen > 0  )
			rememberqueen -= 1;
		if(multi )
		{
			game->turn+=1;
			game->turn%=2;
		}
		striker->reinitialise();
	}
}

void update_striker(void )
{
	if( striker->vel != 0.0f ){
		if( fabs(striker->vel_x) > fabs( game->friction*striker->vel_x / striker->vel ) )
			striker->vel_x -= game->friction*striker->vel_x / striker->vel;
		else 
			striker->vel_x = 0.0f;

		if(fabs(striker->vel_y) > fabs(game->friction*striker->vel_y / striker->vel))
			striker->vel_y -= game->friction*striker->vel_y / striker->vel;
		else
			striker->vel_y = 0.0f;

		striker->vel = sqrt(pow(striker->vel_x,2) + pow(striker->vel_y,2));
		if(striker->vel < 0.01f )
			striker->update_vel(0.0f, 0.0f);
		if( striker->vel )
			reinit=1;
		striker->x+=striker->vel_x;
		striker->y+=striker->vel_y;
		if( striker->x > 1.39f || striker->x < -1.39f )
			striker->vel_x *= -1;

		if( striker->y > 1.39f || striker->y < -1.39f )
			striker->vel_y *= -1;

	}
	if( pow(fabs(striker->x )-1.35f, 2)+ pow(fabs(striker->y)-1.35f,2) <= pow(2*game->rad,2))
	{
		striker->update_pos(5.0f,5.0f);
		striker->update_vel(0.0f,0.0f);
		game->points[game->turn + 1]-=10;
	}

}


int check_collision(Coin* c1, Coin* c2, int i){
	
	double dist = sqrt(pow(c1->x - c2->x ,2) + pow( c1->y - c2->y,2));
	double dist2 = sqrt(pow(c1->x - c2->x + c1->vel_x - c2->vel_x ,2 ) + pow( c1->y - c2->y + c1->vel_y - c2->vel_y ,2));
	double rad;
	if( i == 2 )
		rad = 2*game->rad;
	else
		rad = game->rad + striker->rad;
	if( dist < dist2 && dist <= rad  && (c1->vel || c2->vel))
		return 1;
	else return 0;

}


void collide(void)
{
	int i , j;
	for(i=0; i < game->coins; i++)
	{
		if( check_collision(game->white[i], striker ,1))
			collision(game->white[i],striker,1,2);
		if( check_collision(game->black[i], striker ,1))
			collision(game->black[i],striker,1,2);
	}
	if( rememberqueen!=-5)
		if( check_collision(game->queen, striker ,1))
			collision(game->queen,striker,1,2);

	for( i = 0; i< game->coins; i++ ){
		if( rememberqueen!=-5)
		{
			if( check_collision(game->white[i], game->queen,2))
				collision(game->white[i],game->queen,1,1);
			if( check_collision(game->queen, game->black[i],2))
				collision(game->queen, game->black[i],1,1);
		}
		for(j = 0; j< game->coins ; j++){
			if( i < j ){
				if( check_collision(game->white[i],game->white[j] ,2) )
					collision(game->white[i], game->white[j],1,1);
				if( check_collision(game->black[i], game->black[j],2))
					collision(game->black[i],game->black[j],1,1);
			}
			if( check_collision(game->white[i], game->black[j],2))
				collision(game->white[i], game->black[j],1,1);
		}
	}
}


void update(int value){
	int i;
	for(i=0;i<game->coins; i++)
	{
		apply_friction(game->white[i]);
		change_coords(game->white[i]);
		check_hole(1,game->white[i]);
		apply_friction(game->black[i]);
	        change_coords(game->black[i]);
		check_hole(2,game->black[i]);
	}
	
	apply_friction(game->queen);
	change_coords(game->queen);
	check_hole(3,game->queen);

	update_striker();
	check_reinitialise();
	collide();

}

long long int last = 0;
void checkTime(int val){

	gettimeofday(&tv_now, NULL);
	if(abs(tv_now.tv_usec - tv_prev.tv_usec) >= 15 )
	{
		tv_prev.tv_usec = tv_now.tv_usec;
		if(( tv_now.tv_sec - tv_init.tv_sec) % 5 == 0 && tv_now.tv_sec - tv_init.tv_sec != last && !multi )	
		{	
			game->points[1] -=1;
			last = tv_now.tv_sec - tv_init.tv_sec;
		}
		update(1);
	}
	if( ( multi &&(game->coins > game->count[1] || game->coins > game->count[2] ))|| (!multi && game->coins > game->count[1] ) || game->coins == 0 )
		glutTimerFunc(15, checkTime, 0);

}

void drawBox(float len) {
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glBegin(GL_QUADS);
	glVertex2f(-len / 2, -len / 2);
	glVertex2f(len / 2, -len / 2);
	glVertex2f(len / 2, len / 2);
	glVertex2f(-len / 2, len / 2);
	glEnd();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void drawBall(float rad) {

	glBegin(GL_TRIANGLE_FAN);
	for(int i=0 ; i<360 ; i++) {
		glVertex2f(rad * cos(DEG2RAD(i)), rad * sin(DEG2RAD(i)));
	}
	glEnd();
}

void drawTriangle() {

	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);
	glEnd();
}

// Initializing some openGL 3D rendering options
void initRendering() {

	glEnable(GL_DEPTH_TEST);        // Enable objects to be drawn ahead/behind one another
	glEnable(GL_COLOR_MATERIAL);    // Enable coloring
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);   // Setting a background color
}

// Function called when the window is resized
void handleResize(int w, int h) {

	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (float)w / (float)h, 0.1f, 200.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void handleKeypress1(unsigned char key, int x, int y) {

	if(yes){
		if (key == 27) {
			striker->reinitialise();
			//        exit(0);     // escape key is pressed
		}
		else if (key == ' ' && striker->vel_x == 0 && striker->vel_y == 0 && striker->y == -0.96f) {
			striker->shoot();
		}
		else if (key == 'c' &&  striker->vel_x == 0 && striker->vel_y == 0 && striker->y == -0.96f){
			striker->angle+=10;
			if(striker->angle > 90 )
				striker->angle= 90;
			striker->show_angle();
		}
		else if (key == 'a' &&   striker->vel_x == 0 && striker->vel_y == 0 && striker->y == -0.96f){
			striker->angle-=10;;
			if(striker->angle < -90 )
				striker->angle = -90;
			striker->show_angle();
		}
		else if( game->coins == game->count[1] || game->coins == game->count[2] ){
			if( key == 'y' )
			{
				rememberqueen = 0;
				p = 0;
				game = new GamePlay(0,0.0001,40);
				yes = 0;
				glutTimerFunc(15, checkTime, 0);

			}
			else if( key == 'n' )
				exit(0);
		}
	}
	else {
		if( key == 'o' || key =='t' ){
			p =1;
			if( key == 'o' )
				multi = 0;
			else multi = 1;

		}
		if((key == 'w'|| key == 'b' )&& p  && game->color == 0)
		{
			gettimeofday(&tv_now, NULL);
			int t = tv_now.tv_sec;
			while( abs(tv_now.tv_sec - t )< 2 )
			{
				glMatrixMode(GL_MODELVIEW);
				glLoadIdentity();
				if(key == 'w')
				{
					game->color = 1;
					drawText("You take White",-3.40f, -1.0f,-8.0f);
				}
				else{
					game->color = 2;
					drawText("You take Black",-3.40f, -1.0f,-8.0f);
				}	
				gettimeofday(&tv_now, NULL);
				glutSwapBuffers();
			}
		}
		else if(key >= '3' && key<= '6' && game->color)
		{
			game->coins = key-'0';

		}
		else if( key == 'l')
		{
			yes = 1;
		}
		else if( key == 'm' )
		{
			yes = 1;
			game->friction*=2;
		}
		else if( key == 'h')
		{
			yes = 1;
			game->friction *=4;
		}
		game->points[1]=40;
		game->points[2]=40;
		gettimeofday(&tv_prev, NULL);
		gettimeofday(&tv_init, NULL); 
	}
}

double d( Coin* c){
	return sqrt( pow(c->x-striker->x,2)+pow(c->y-striker->y,2));
}

void handleKeypress2(int key, int x, int y) {
	if(  striker->vel_x == 0 && striker->vel_y == 0 && striker->y == -0.96f && yes){
		if (key == GLUT_KEY_LEFT)
		{
			striker->x-=0.1;
			if( striker->x < -0.96f + striker->rad)
				striker->x = -0.96f + striker->rad;
		}
		if (key == GLUT_KEY_RIGHT)
		{
			striker->x+=0.1;
			if( striker->x > 0.96f - striker->rad )
				striker->x = 0.96f - striker->rad;
		}
		if (key == GLUT_KEY_UP  )
		{
			striker->fake_vel += 0.01;
			if( striker->fake_vel > 0.06f )
				striker->fake_vel = 0.06f;
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			striker->show_vel();
			glutSwapBuffers();
		}
		if (key == GLUT_KEY_DOWN)
		{
			striker->fake_vel -= 0.01;
			if( striker->fake_vel < 0.01f )
				striker->fake_vel = 0.01f;
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			striker->show_vel();
			glutSwapBuffers();

		}
	}
}

void myMouse(int b, int s, int x, int y) {      // mouse click callback
	if (s == GLUT_DOWN) {
		if (b == GLUT_RIGHT_BUTTON) {
			active=1;
		}	
		if (b == GLUT_LEFT_BUTTON && striker->vel_x == 0 && striker->vel_y == 0 && striker->y == -0.96f ) {
			active = -1;
			up=y;
			striker->angle=atan(((float)(x-455)/100.0f-striker->x )/ ((float)(256.5-y)/100.0f-striker->y))*180.0f/PI;
			striker->fake_vel +=0.010;
		}
	}
	if( s == GLUT_UP)
	{
		if( active == -1 )
		{
			striker->shoot();
		}

		active = 0;
	}	
}

void handleMouseclick(int button, int state, int x, int y) {

	if (state == GLUT_DOWN)
	{
		if (button == GLUT_RIGHT_BUTTON)
			striker->x = (float)(x-455)/(float)85;
	}
}

void motion( int x, int y )
{
	if( active == 1){
		striker->x =  (float)(x-456.5)/(float)105;
		if( x > 541 )
		{
			striker->x = (float)(541-465.5)/float(90);
		}
		else if ( x <373 )
		{
			striker->x = (float)(373-465.5)/float(110);
		}
	}

	if( active == -1 )
	{
		if( y < up )

			striker->fake_vel +=0.010;
		else
			striker->fake_vel -=0.010;

		if( striker->fake_vel > 0.08f )
			striker->fake_vel = 0.08f;

		if( striker->fake_vel < 0.01f )
			striker->fake_vel = 0.01f;

		up = y;
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		striker->show_vel();
		striker->show_angle();
		glutSwapBuffers();
		glutPostRedisplay();
	}
}
