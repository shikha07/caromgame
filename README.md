"My Carom" is a simple game based on carom.
Rules are pretty simple:

Single player:
------------------------------------------------------

1 player chooses the color he wants to play for
2 numbers of coins of each color
3 difficulty level ie roughness of surface
4 timer
5 score decrements by one every 5 seconds
6 necessary to pocket the queen before pocketing all coins of his color
7 other color's coin placed at center if pocketed
8 displays scores , coins pocketed


Multi player( 2 players ):
------------------------------------------------------

1 player1 chooses color
2 player1 initiates
3 displays turn of player, scores , coins pocketed
4 each player get 1 strike at a time irrespective of coins pocketed
5 gets to choose difficulty level , number of coins
6 necessary to pocket the queen before pocketing all coins of any color

Points:
-------------------------------------------------------
initally each player get 40 bonus
+10 for pocketing coin of his color
-5 for pocketing coin of other color
+50 for pocketing coin of queen
-10 for pocketing coin of striker

Moves:
-------------------------------------------------------
Keyboard inputs:

a to change direction anticlockwise
c to change the direction clockwise
left key to move striker leftwards
right key to move striker rightwards
spacebar to shoot

Mouse inputs:

left button down : set direction
left button up : shoot
right button down : move the striker horizontally 
cursor up and down : increase decrease speed 



***ignore warnings on compilation***